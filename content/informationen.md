---
title: 'Informationen'
title-meta: 'Informationen'
image:
  file: bild-krankenkassen.jpg
  description: Bild einer Orchidee
  lighten: true
---

# Termine

nach Vereinbarung (telefonisch oder per E-Mail)

# Dauer

60–90 Minuten

# Kosten

126 Fr. für 60 Minuten

# Krankenkasse

Ich bin von den meisten Krankenkassen anerkannt (Mitglied vom EMR). Kinesiologie wird über die Zusatzversicherung vergütet.

Bitte erkundigen Sie sich vor Beginn der Behandlung bei Ihrer Kasse, ob und wie Sie für Kinesiologie versichert sind.

> Hinweis: Vereinbarte Termine können ohne Kosten 24 Stunden vorher abgesagt werden.
> Bei kurzfristigen Absagen wird eine halbe Stunde in Rechnung gestellt, bei Nichterscheinen wird eine Stunde verrechnet.
