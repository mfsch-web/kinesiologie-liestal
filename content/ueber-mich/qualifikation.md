---
title: 'Rosmarie Flückiger Schmid'
title-meta: 'Über mich'
image:
  file: bild-uebermich.jpg
  description: "Porträt Rosmarie Flückiger Schmid"
---

- Ausbildung als Primarlehrerin und als Heilpädagogin / Sonderklassenlehrerin
- mehrjährige Erfahrung als Dyskalkulie- und Legasthenietherapeutin
- Ausbildung zur Kinesiologin, seit 1993 Arbeit in der eigenen Praxis
- Erfahrung in Meditation und Gewaltfreier Kommunikation nach Marshall Rosenberg
- Coach für The Work of Byron Katie (vtw)
- Mitglied des Berufsverbandes KineSuisse
- Anerkannt vom EMR und von der EGK
- verheiratet, Mutter von drei erwachsenen Söhnen
