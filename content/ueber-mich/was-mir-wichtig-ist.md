---
title: 'Was mir wichtig ist'
title-meta: 'Was mir wichtig ist'
image:
  file: bild-uebermich.jpg
  description: "Porträt Rosmarie Flückiger Schmid"
---

Als Kinesiologin behandle ich nicht einzelne Symptome, sondern den ganzen Menschen. Meine Arbeit unterstützt die Selbstheilungskräfte in Körper, Geist und Seele.

Krankheit und Schmerzen sind Signale, dass etwas in uns aus dem Gleichgewicht geraten ist. Ich lade Menschen ein, Krisen als hilfreiche Wachstumsmöglichkeit zu entdecken.

Es gibt ein inneres Wissen, wer wir sind und welche Möglichkeiten in uns schlummern. Ich begleite Erwachsene und Kinder auf dem Weg, mit diesem Wissen in Kontakt zu kommen.

Lernen, ob im Leben oder in der Schule, geschieht leicht und freudig, wenn wir offen sind und frei von Leistungs- und Erwartungsdruck. Ich helfe Kindern und Erwachsenen ihre ursprüngliche Lernfreude wieder zu finden.

Herausforderungen, die uns begegnen, können uns lähmen oder beflügeln. Ich unterstütze Menschen darin, über ihre Grenzen hinauszuwachsen.

Ich ermutige Kinder und Erwachsene, Verantwortung für die eigene Gesundheit zu übernehmen. Sie lernen einfache Übungen, mit deren Hilfe sie sich in kritischen Situationen selber ins Gleichgewicht bringen können.
