---
title: 'Willkommen'
---

Kinesiologie gehört zu den komplementären Heilmethoden und verbindet westliches und östliches medizinisches Wissen. [>> Kinesiologie](/kinesiologie/was-ist-kinesiologie)

In meiner Kinesiologie-Praxis begleite ich Erwachsene, Jugendliche und Kinder mit unterschiedlichen Beschwerden und Anliegen. [>> Für wen geeignet?](/kinesiologie/fuer-wen-geeignet)

Als ehemalige Lehrerin und Heilpädagogin habe ich mich speziell mit Lernen und dem Lösen von Lernblockaden beschäftigt. [>> Lernen](/kinesiologie/lernen)

Ich sehe meine Aufgabe darin, Menschen zu Lebensfreude, Selbstvertrauen und zum Entdecken des eigenen Potentials zu ermutigen. Kinesiologie stärkt die Selbstheilungskräfte im Menschen. [>> Über mich](/ueber-mich/qualifikation)

Ich freue mich, wenn Sie mit mir in Kontakt treten. [>> Kontakt](/kontakt)
