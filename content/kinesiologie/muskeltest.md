---
title: 'Der Muskeltest'
title-meta: 'Der Muskeltest'
image:
  file: bild-muskeltest.jpg
  description: Bild einer Orchidee
  lighten: true
---

Unsere Muskeln reagieren in jedem Moment auf das, was wir gerade denken, fühlen und erleben. Stress zeigt sich in der Muskulatur entweder durch erhöhte Anspannung oder durch Kraftlosigkeit. So spiegelt sich zum Beispiel der Energiefluss der Meridiane (Energiebahnen) im Zustand bestimmter Muskelgruppen.

Der Muskeltest ist mein Arbeitsinstrument. Er ist eine Art Biofeedback. Mit seiner Hilfe lassen sich Energieblockaden aufspüren und ausgleichen.

Der Muskeltest schafft die Verbindung zu einem inneren Wissen, das in jedem Mensch vorhanden ist. In der kinesiologischen Arbeit dient der Test als „Reisekompass“.
