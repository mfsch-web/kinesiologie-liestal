---
title: 'Für wen ist Kinesiologie geeignet?'
title-meta: 'Für wen geeignet?'
image:
  file: bild-zielpublikum.jpg
  description: Bild einer Orchidee
  lighten: true
---

Als Kinesiologin begleite ich Kinder, Jugendliche und Erwachsene. Die Themen und Beschwerden, mit denen Menschen mich aufsuchen, sind vielfältig. Hier einige Beispiele:

# Erwachsene

- Erschöpfung, Freudlosigkeit, Burn-out
- Ängste und Phobien (z.B. Flugangst, Platzangst, Angst vor Spinnen)
- Essprobleme
- körperliche Beschwerden und Schmerzen aller Art, bei denen keine klare medizinische Ursache zu finden ist
- Vorbereitung auf eine Operation, begleitende Unterstützung einer medizinischen Behandlung

<!-- -->

- Kinderwunsch (Begleitung der Frau oder des Paares)
- Geburtsvorbereitung (Stressabbau)

<!-- -->

- Lebenskrisen wie Trennung, Ablösung der Kinder, berufliche Neuorientierung
- Paarkonflikte
- Erziehungsfragen

<!-- -->

- Lern-, Gedächtnis- und Konzentrationsprobleme

# Jugendliche

- Krisen während der Ablösung vom Elternhaus
- Selbstwertprobleme
- Probleme in der Lehre oder in weiterführenden Schulen

# Schulkinder

- Verdacht auf ADHS
- Lernblockaden
- Konzentrationsschwäche
- Hyperaktivität

<!-- -->

- Ängste
- Prüfungsversagen
- Selbstwertprobleme

<!-- -->

- Mobbing
- soziale Schwierigkeiten
- Gewalt

# Kleine Kinder

Bei kleinen Kindern äussert sich Stress oft körperlich, zum Beispiel als Bauch- oder Kopfschmerzen, als Schlaf- oder Essproblem. Wutausbrüche oder Zurückgezogenheit weisen oft auf eine innere Not hin.

Kinder, die selber noch nicht testbar sind, können über eine andere Person, zum Beispiel die Mutter, getestet werden.

# Elternberatung

Eltern und Kinder sind energetisch eng miteinander verbunden. Probleme der Kinder lösen bei den Eltern Stress und Zweifel an den eigenen Fähigkeiten aus. Kinder spüren die Sorgen der Eltern als Druck, der auf ihnen lastet.

Oft ist es hilfreich, wenn Eltern für sich selber Unterstützung bekommen.

Wenn Sie unsicher sind, ob Kinesiologie Sie in ihrem Anliegen unterstützen kann, rufen Sie mich an. [>> Kontakt](/kontakt)

[nach oben](#start)
