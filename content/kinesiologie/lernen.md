---
title: 'Lernen'
title-meta: 'Lernen'
image:
  file: bild-lernen.jpg
  description: Bild einer Orchidee
  lighten: true
---

Ein kleines Kind lernt lustvoll und leicht. In der Schule verlieren manche Kinder ihre ursprüngliche Lernfreude. Die Gehirnforschung zeigt, dass Leistungs- und Erwartungsdruck und die damit verbundene Angst das Gehirn blockieren.

Aus eigener Erfahrung weiss ich, dass lustvolles Lernen möglich und lernbar ist. Es macht mir grosse Freude, wenn Erwachsene und Kinder dies mit meiner Hilfe wieder entdecken.

Brain Gym-Übungen (Gehirn-Gymnastik) können uns dabei wirkungsvoll unterstützen.

In meiner Arbeit verbinde ich meine pädagogische Erfahrung mit den Erkenntnissen der modernen Gehirnforschung.
