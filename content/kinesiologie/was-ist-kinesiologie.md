---
title: 'Kinesiologie – Neue Möglichkeiten entdecken…'
title-meta: 'Was ist Kinesiologie?'
image:
  file: bild-kinesiologie.jpg
  description: Bild einer Orchidee
  lighten: true
---

Kinesiologie verbindet altes Wissen aus dem Osten mit Erkenntnissen der westlichen Medizin, der modernen Gehirnforschung und der Psychologie.

Gesund fühlen wir uns, wenn die Energie im Körper frei fliesst. In jedem Moment pendelt sich das energetische Gleichgewicht ein, ohne dass wir darüber nachdenken müssen.

Manchmal stehen wir vor Herausforderungen, die uns aus der Bahn werfen, und schaffen es nicht, uns selber wieder ins Gleichgewicht zu bringen.Wir fühlen uns erstarrt und blockiert.

Mit dem kinesiologischen [Muskeltest](/kinesiologie/muskeltest) lässt sich herausfinden, wo unsere Energie blockiert ist und was wir brauchen, um den Energiefluss zu harmonisieren und die Selbstheilungskräfte zu aktivieren.

Kinesiologie ist wie eine Forschungsreise in noch unentdeckte Landschaften. Neugier, Offenheit und Abenteuerlust sind ein gutes Reisegepäck. Ich freue mich, Sie auf Ihrer Reise zu begleiten.
