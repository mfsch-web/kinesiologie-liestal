---
title: 'Kontakt und Anmeldung'
title-meta: 'Kontakt'
map: true
---

Rosmarie Flückiger Schmid  
Praxis für Kinesiologie  
Kirchbergweg 1  
CH-4415 Lausen BL

Telefon: +41 (0)61 921 19 61  
E-Mail: `<script type='text/javascript'>var a = new Array('ie-liestal.ch','info@kinesiolog');document.write("<a class='email' href='mailto:"+a[1]+a[0]+"' title='Schreiben Sie mir'>"+a[1]+a[0]+"</a>");</script><noscript><em>Aktivieren Sie JavaScript, um die E-Mailadresse zu sehen.</em></noscript>`{=html}

Ich freue mich über Ihren Anruf oder Ihre E-Mail.

Die Praxis befindet sich im Erdgeschoss des Hauses.

# Anreise mit Bus und Bahn

S3 bis Lausen, dann 10-15 Minuten zu Fuss oder mit Bus Nr. 78 (Richtung Frenkendorf Friedhof) bis Lausen Kirchstrasse, von dort ca. 3 Minuten zu Fuss.

Vom Bahnhof Liestal Bus Nr. 78 (Richtung Lausen Stutz) bis Lausen Kirchstrasse.

# Anreise mit dem Auto

Den Wegweisern Kirche, Friedhof folgen. Parkplatz beim Haus.
