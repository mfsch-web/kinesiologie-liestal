.DEFAULT_GOAL := site
.PHONY: site clean

SITE = public
TOOLS = tools

PANDOC_VERSION = 2.9.2
pandoc = $(TOOLS)/pandoc-$(PANDOC_VERSION)/bin/pandoc

content = $(patsubst content/%.md,/%,$(wildcard content/*.md) $(wildcard content/*/*.md))
$(info content: $(content))

site: $(SITE)/css/main.css \
    $(patsubst %,$(SITE)%.html,$(content)) \
    $(SITE)/img/bild-index.jpg \
    $(SITE)/img/bild-kinesiologie.jpg \
    $(SITE)/img/bild-muskeltest.jpg \
    $(SITE)/img/bild-lernen.jpg \
    $(SITE)/img/bild-zielpublikum.jpg \
    $(SITE)/img/bild-uebermich.jpg \
    $(SITE)/img/bild-herangehensweise.jpg \
    $(SITE)/img/bild-krankenkassen.jpg \
    $(SITE)/favicon.ico \
    $(SITE)/Datenschutzerklaerung-230901.pdf \
    $(SITE)/BingSiteAuth.xml

$(SITE)/img/%: img/%
	mkdir -p $(SITE)/img
	cp $< $@

$(SITE)/%.pdf: %.pdf
	cp $< $@

$(SITE)/favicon.ico: img/favicon.ico
	cp $< $@

clean:
	rm -rf $(SITE) $(TOOLS)

$(SITE) $(TOOLS) $(SITE)/kinesiologie:
	mkdir -p $@

$(TOOLS)/dart-sass/sass: | $(TOOLS)
	wget -O $(TOOLS)/dart-sass.tar.gz https://github.com/sass/dart-sass/releases/download/1.24.2/dart-sass-1.24.2-linux-x64.tar.gz
	tar -xzf $(TOOLS)/dart-sass.tar.gz -C $(TOOLS)

$(pandoc): | $(TOOLS)
	wget -O $(TOOLS)/pandoc.tar.gz https://github.com/jgm/pandoc/releases/download/$(PANDOC_VERSION)/pandoc-$(PANDOC_VERSION)-linux-amd64.tar.gz
	tar -xzf $(TOOLS)/pandoc.tar.gz -C $(TOOLS)

$(SITE)/%.html: content/%.md template.html | $(pandoc)
	mkdir -p $(dir $@)
	$(pandoc) --template=template.html --shift-heading-level-by=1 $(subst /, --variable=nav-,/$*) --output=$@ $<

$(SITE)/css/main.css: styles/main.scss styles/*.scss | $(SITE) $(TOOLS)/dart-sass/sass
	mkdir -p $(SITE)/css
	tools/dart-sass/sass $< $@

$(SITE)/BingSiteAuth.xml:
	echo -ne '<?xml version="1.0"?>\n<users>\n\t<user>3503956196A5C281B17724638FC972CA</user>\n</users>' > $@
